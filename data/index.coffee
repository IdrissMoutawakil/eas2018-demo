index =

	features:
		vuforia:
			database: 'EAS2018.xml'
		homekit:
			devices: [
				'leFeu'
			]
	startPhase: 'demoHome'
	stylesheet: 'custom.css'
	variables: require('./variables')

	phases:
		demoHome:
			type:'quest'
			backgroundImage: './locations/Home-Bouton.jpg'
			locations: require('./locations')
			entities: require('./entities')
			startup:
				updates:
					addToInventory:[
						'boutonRepondre'
						'boutonARview'
					]
		test:
			type:'dialogue'
			entities: require('./entities')
			locations:require('./locations')
			location:'black'
			scenes:[
				type:'arview'
				classes:'AutoPlay'
				overlayMapping:
					Yoda:
						overlayObject:'Yoda.obj'
					Tuyaux_A:
						overlayObject:'Tuyaux_A.obj'
					Tuyaux_B:
						overlayObject:'Tuyaux_B.obj'
					Tuyaux_C:
						overlayObject:'Tuyaux_C.obj'
				forceSingleView:true
				autoplay:true
			,
				type:'empty'
				location:'black'
				updates:
					changePhase:'demoHome'
			]

module.exports = index
